main:
	g++ -std=c++0x WhereToEat.cpp -fPIC -lfastcgi-daemon2 -L/usr/local/lib/ -lrestclient-cpp -shared -o libwheretoeat.so
run:
	fastcgi-daemon2 --config=wheretoeat.conf
clean:
	rm *.so
