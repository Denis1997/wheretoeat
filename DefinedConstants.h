#ifndef DEFINED_CONSTANTS_H
#define DEFINED_CONSTANTS_H

#include <string>

const std::string PLACES = "/v1/places/";
const std::string COMMENTS = "/v1/comments/";
const std::string RATINGS = "/v1/ratings/";
const std::string ROOT = "/";

const std::string GET = "GET";
const std::string PUT = "PUT";
const std::string POST = "POST";
const std::string DELETE = "DELETE";

const std::string CouchDbServer = "http://localhost:5984";

const std::string DB_PLACES = "/places/";
const std::string COMMENT_PACKAGES = "/comment_packages/";

#define COMMENT_CASE_SIZE 20
#define HTTP_OK 200
#define HTTP_NOT_FOUND 404
#define HTTP_CREATED 201

#define PLACE_NOT_EXISTS -1
#endif
