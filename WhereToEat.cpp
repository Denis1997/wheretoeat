#include <fastcgi2/component.h>
#include <fastcgi2/component_factory.h>
#include <fastcgi2/handler.h>
#include <fastcgi2/request.h>

#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

#include "restclient-cpp/connection.h"
#include "restclient-cpp/restclient.h"

#include "DefinedConstants.h"

#include "RootController.h"
#include "RatingsController.h"
#include "PlacesController.h"
#include "CommentsController.h"



#include <iostream>
#include <cstdio>
#include <string>
using namespace rapidjson;

class WhereToEatService: virtual public fastcgi::Component, virtual public fastcgi::Handler {

public:
        WhereToEatService(fastcgi::ComponentContext *context) :
                fastcgi::Component(context) {
        }
        virtual ~WhereToEatService() {
        }

public:
        virtual void onLoad() {
        }
        virtual void onUnload() {
        }
        virtual void handleRequest(fastcgi::Request *request, fastcgi::HandlerContext *context) {
			std::string uri = request->getURI();
			if (isRoute(uri, PLACES))
			{
				PlacesController pc;
				pc.Handle(request, context);
				return;
			}
			if (isRoute(uri, COMMENTS))
			{
				CommentsController cc;
				cc.Handle(request, context);
				return;
			}
			if (isRoute(uri, RATINGS))
			{
				RatingsController rc;
				rc.Handle(request, context);
				return;
			}
			if (isRoute(uri, ROOT))
			{
				RootController rc;
				rc.Handle(request, context);
				return;
			}
			char ans[100];
			int len = sprintf(ans, "%s", request->getURI().c_str());
			request->write(ans, len);
        }
private:
		bool isRoute(std::string& uri, const std::string& pattern)
		{
			//std::string::const_iterator res = std::mismatch(pattern.begin(), pattern.end(), uri.begin()).first;
			//if (res == pattern.end())
			//	return true;
			if(uri.find(pattern) == 0)
				return true;
			return false;
		}
};

FCGIDAEMON_REGISTER_FACTORIES_BEGIN()
FCGIDAEMON_ADD_DEFAULT_FACTORY("wheretoeat_factory", WhereToEatService)
FCGIDAEMON_REGISTER_FACTORIES_END()
