#ifndef RATINGS_CONTROLLER_H
#define RATINGS_CONTROLLER_H

#include <fastcgi2/request.h>

#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

#include "restclient-cpp/connection.h"
#include "restclient-cpp/restclient.h"

#include <cstdio>
#include <iostream>
#include <sstream>
#include <string>

using namespace rapidjson;

//uri is "/v1/ratings/"
class RatingsController
{
	public:
		RatingsController(){}
		virtual ~RatingsController(){}
		
		void Handle(fastcgi::Request *request, fastcgi::HandlerContext *context)
		{
			std::string requestMethod= request->getRequestMethod();
			
			if (requestMethod.compare(GET) == 0)
			{
				HandleGet(request, context);
				return;
			}
			if (requestMethod.compare(POST) == 0)
			{
			   	TryUpdateRating(request, context);
				return;
			}
			if (requestMethod.compare(PUT) == 0)
			{
			   	TryUpdateRating(request, context);
				return;
			}

			std::stringbuf buffer3("Unknown request.\n");
                	request->write(&buffer3);
		}

	private:
		void HandleGet(fastcgi::Request *request, fastcgi::HandlerContext *context)
		{
			try 
			{
				std::string uri= request->getURI();
				int ind = uri.rfind("/");
				std::string id = uri.substr(ind + 1); 
				WritoOutRating(request, id);
				return;
			} catch (...) 
			{ 
				std::stringbuf buffer2("Some server error.\n");
                		request->write(&buffer2);
				return;
			}	
		}
		
		void TryUpdateRating(fastcgi::Request *request, fastcgi::HandlerContext *context)
		{
			try 
			{
				std::string uri= request->getURI();
				int ind = uri.rfind("/");
				std::string markStr = uri.substr(ind + 1); 
				int ind2 = uri.substr(0, ind).rfind("/");
				std::string id = uri.substr(ind2 + 1, ind - ind2); 
				RestClient::Response r = RestClient::get(CouchDbServer + DB_PLACES + id);
				if(r.code == 200)
				{
					request->setContentType("application/com.wheretoeat.place.rating+json");

					if(markStr.compare("1") && markStr.compare("2") && markStr.compare("3") && markStr.compare("4") && markStr.compare("5"))
					{
						request->setStatus(400);
						std::stringbuf invalidRatingMes("Invalid request. Mark must be in [1, 5].\n");
                				request->write(&invalidRatingMes);
						return;
					}
					markStr = "mark_" + markStr;
					Document place;
					place.Parse(r.body.c_str());
					int mark = place[markStr.c_str()].GetInt();
					mark++;
					Document d;
					d.Parse(r.body.c_str());

					d.RemoveMember(StringRef(markStr.c_str()));
					d.AddMember(StringRef(markStr.c_str()), Value().SetInt(mark), d.GetAllocator());
					
					StringBuffer buf;
     					Writer<StringBuffer> writer(buf);
     					d.Accept(writer);

					RestClient::Response putResp = RestClient::put(CouchDbServer + DB_PLACES + id, "text/json", buf.GetString());
					request->setStatus(putResp.code);
					if(putResp.code == 200 || putResp.code == 201)
					{ 
						WritoOutRating(request, id);
						return;
					}
					else
					{ 
						std::stringbuf buffer2("Some server error while updatind rating.\n");
                				request->write(&buffer2);
						return;
					}

				}
				else
					WriteOutRequest(request, r);
				return;
			} catch (...) 
			{ 
				std::stringbuf buffer2("Some server error.\n");
                		request->write(&buffer2);
				return;
			}
		}
	
		void WriteOutRequest(fastcgi::Request *request, RestClient::Response r)
		{
			Document d;
			d.Parse(r.body.c_str());
			StringBuffer buf;
     			Writer<StringBuffer> writer(buf);
     			d.Accept(writer);
			std::stringbuf buffer(std::string(buf.GetString()) +"\n");
                	request->write(&buffer);
			return;
		}
		void WritoOutRating(fastcgi::Request *request, std::string id)
		{
			RestClient::Response r = RestClient::get(CouchDbServer + DB_PLACES + id);
			request->setStatus(r.code);
			request->setContentType("application/com.wheretoeat.place.rating+json");
			if(r.code == 200)
			{
				Document place;
				Document mark;
				place.Parse(r.body.c_str());
				mark.Parse("{}");
				int mark_1 = place["mark_1"].GetInt();
				int mark_2 = place["mark_2"].GetInt();
				int mark_3 = place["mark_3"].GetInt();
				int mark_4 = place["mark_4"].GetInt();
				int mark_5 = place["mark_5"].GetInt();
				int voters_count = mark_1 + mark_2 + mark_3 + mark_4 + mark_5;
				double average_mark = 0;
				if(voters_count != 0)
					average_mark = (mark_1 * 1 + mark_2 * 2 + mark_3 * 3 + mark_4 * 4 + mark_5 * 5) / (double)voters_count;
				mark.AddMember("voters_count", Value().SetInt(voters_count), mark.GetAllocator());
				mark.AddMember("average_mark", Value().SetDouble(average_mark), mark.GetAllocator());
				StringBuffer buf;
     				Writer<StringBuffer> writer(buf);
     				mark.Accept(writer);
				std::stringbuf buffer(std::string(buf.GetString()) +"\n");
                		request->write(&buffer);				
			}
			else
				WriteOutRequest(request, r);
			return;
		}
		

};
#endif