#ifndef PLACES_CONTROLLER_H
#define PLACES_CONTROLLER_H

#include <fastcgi2/request.h>

#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

#include "restclient-cpp/connection.h"
#include "restclient-cpp/restclient.h"

#include "DefinedConstants.h"

#include <cstdio>
#include <iostream>
#include <sstream>
#include <string>

using namespace rapidjson;

//uri is "/v1/places"
class PlacesController
{
	public:
		PlacesController(){}
		virtual ~PlacesController(){}
		
		void Handle(fastcgi::Request *request, fastcgi::HandlerContext *context)
		{
			std::string requestMethod= request->getRequestMethod();
			
			if (requestMethod.compare(GET) == 0)
			{
				HandleGet(request, context);
				return;
			}
			if (requestMethod.compare(POST) == 0)
			{
			   	HandlePost(request, context);
				return;
			}
			std::stringbuf buffer3("Unknown request.\n");
                	request->write(&buffer3);
		}
	private:
		void HandleGet(fastcgi::Request *request, fastcgi::HandlerContext *context)
		{
			std::string uri= request->getURI();
			int ind = uri.rfind("/");
			std::string id = uri.substr(ind + 1); 
			RestClient::Response r = RestClient::get(CouchDbServer + DB_PLACES + id);
			request->setStatus(r.code);
			WhiteOutRequest(request, r);
		}
		
		void HandlePost(fastcgi::Request *request, fastcgi::HandlerContext *context)
		{
			try 
			{
				fastcgi::DataBuffer dataBuffer = request->requestBody();
				RestClient::Response uuidResp = RestClient::get(CouchDbServer + "/_uuids");
				Document uuidDoc;
				uuidDoc.Parse(uuidResp.body.c_str());
				std::string uuid = uuidDoc["uuids"][0].GetString();
				
				std::string placeStr;
				dataBuffer.toString(placeStr);
				Document placeDoc;
				placeDoc.Parse(placeStr.c_str());
				if(!placeDoc.IsObject() || !placeDoc.HasMember("name") || !placeDoc.HasMember("type")
					|| !placeDoc.HasMember("lat") || !placeDoc.HasMember("lon"))
				{
					request->setStatus(400);
					std::stringbuf invalidPlaceMes("Invalid JSON. Example: {\"type\":2,\"lat\":59.776952,\"lon\":32.389409,\"name\":\"My Bar\"}\n");
                			request->write(&invalidPlaceMes);
					return;
				}
				Value uri;
				std::string uriStr = PLACES + uuid;
				const char * uriStrC = uriStr.c_str();
				uri.SetString(StringRef(uriStrC));

				placeDoc.AddMember("uri", uri, placeDoc.GetAllocator());
				placeDoc.AddMember("mark_1", Value().SetInt(0), placeDoc.GetAllocator());
				placeDoc.AddMember("mark_2", Value().SetInt(0), placeDoc.GetAllocator());
				placeDoc.AddMember("mark_3", Value().SetInt(0), placeDoc.GetAllocator());
				placeDoc.AddMember("mark_4", Value().SetInt(0), placeDoc.GetAllocator());
				placeDoc.AddMember("mark_5", Value().SetInt(0), placeDoc.GetAllocator());
				
				StringBuffer buf;
     				Writer<StringBuffer> writer(buf);
     				placeDoc.Accept(writer);

				RestClient::Response putResp = RestClient::put(CouchDbServer + DB_PLACES + uuid, "text/json", buf.GetString());
				request->setStatus(putResp.code);
				if(putResp.code == 201)
				{
					RestClient::Response getResp = RestClient::get(CouchDbServer + DB_PLACES + uuid);
					WhiteOutRequest(request, getResp);
				}
				else
					WhiteOutRequest(request, putResp);

				return;

			} catch (...) 
			{ 
				std::stringbuf buffer2("Some server error.\n");
                		request->write(&buffer2);
				return;
			}
		}
		void WhiteOutRequest(fastcgi::Request *request, RestClient::Response r)
		{
			request->setContentType("application/com.wheretoeat.place+json");

			Document d;
			d.Parse(r.body.c_str());
			d.RemoveMember("_id");
			d.RemoveMember("_rev");
			StringBuffer buf;
     			Writer<StringBuffer> writer(buf);
     			d.Accept(writer);
			std::stringbuf buffer(std::string(buf.GetString()) +"\n");
                	request->write(&buffer);
			return;
		}

};
#endif