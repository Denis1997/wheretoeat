#ifndef ROOT_CONTROLLER_H
#define ROOT_CONTROLLER_H

#include <fastcgi2/request.h>

#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

#include "restclient-cpp/connection.h"
#include "restclient-cpp/restclient.h"

#include <cstdio>
//uri is "/"
class RootController
{
	public:
		RootController(){}
		virtual ~RootController(){}
		
		void Handle(fastcgi::Request *request, fastcgi::HandlerContext *context)
		{
			char ans[20];
			int len = sprintf(ans, "%s", "It's a root\n");
			request->write(ans, len);
		}
	private:
};
#endif