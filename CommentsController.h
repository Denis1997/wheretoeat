#ifndef COMMENTS_CONTROLLER_H
#define COMMENTS_CONTROLLER_H

#include <fastcgi2/request.h>

#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

#include "restclient-cpp/connection.h"
#include "restclient-cpp/restclient.h"

#include "DefinedConstants.h"

#include <cstdio>
#include <vector>
using namespace std;

//uri is "/"
class CommentsController
{
	public:
		CommentsController(){}
		virtual ~CommentsController(){}
		
		void Handle(fastcgi::Request *request, fastcgi::HandlerContext *context)
		{
			string reqMethod = request->getRequestMethod();
			if (reqMethod == GET)
			{
				HandleGet(request, context);
				return;
			}
			if (reqMethod == PUT)
			{
				HandlePut(request, context);
				return;
			}
		}
		
		void HandleGet(fastcgi::Request *request, fastcgi::HandlerContext *context)
		{
			string uri = request->getURI();
			Document d;
			vector<string> uriParts = split(uri, '/');
			if (uriParts.size() == 3)
			{
				string* commentId = &uriParts[2];
				RestClient::Response r = RestClient::get(CouchDbServer + COMMENT_PACKAGES + (*commentId));
				if (r.code != HTTP_OK)
					goto noResource;
				d.Parse(r.body.c_str());
				d.RemoveMember("_id");
				d.RemoveMember("_rev");
				d.RemoveMember("curNum");
				stringbuf buffer(std::string(getStringFromDocument(d)));
				request->write(&buffer);
				return;
			}
			else if (uriParts.size() == 4)
			{
				string* commentPackageId = &uriParts[2];
				string* commentCaseNum = &uriParts[3];
				RestClient::Response r = RestClient::get(CouchDbServer + "/comment_cases/" + (*commentPackageId) + "_" + (*commentCaseNum));
				if (r.code != HTTP_OK)
					goto noResource;
				d.Parse(r.body.c_str());
				d.RemoveMember("_id");
				d.RemoveMember("_rev");
				stringbuf buffer(std::string(getStringFromDocument(d)));
				request->write(&buffer);
				return;
			}
			
			noResource:
			stringbuf buffer("{\"error\":\"no such resource\"}");
			request->write(&buffer);
			
		}
		
		void HandlePut(fastcgi::Request *request, fastcgi::HandlerContext *context)
		{
			string uri = request->getURI();
			vector<string> uriParts = split(uri, '/');
			string text;
			string putAns = "{\"result\":\"Added\"}";
			if (uriParts.size() == 3)
			{
				getCommentText(request, text);			//content of commentary
				string* plcCmtPkgId = &uriParts[2];		//get place and commentPackage id
				int caseNum = getCaseNumber(plcCmtPkgId); //number of case with less than 20 comments
				if (caseNum == PLACE_NOT_EXISTS)
				{
					stringbuf buffer("{\"error\":\"place does not exists\"}");
					request->write(&buffer);
					return;
				}
				while (!tryModifyCase(plcCmtPkgId, caseNum, text))
				{
					updatePackageNumber(plcCmtPkgId, caseNum);
					caseNum = getCaseNumber(plcCmtPkgId);
				}
			}
			request->write(putAns.c_str(), putAns.length());
		}
		
	private:	
		void getCommentText(fastcgi::Request *request, string& text)
		{
			string body;
			request->requestBody().toString(body);
			Document d;
			d.Parse(body.c_str());
			text = d["text"].GetString();
		}
		
		bool tryModifyCase(string* plcCmtPkgId, int caseNum, string& text)
		{
			string caseDbPath = CouchDbServer + "/comment_cases/" +(*plcCmtPkgId) + "_" + to_string(caseNum);
			RestClient::Response r = RestClient::get(caseDbPath);
			Document d;
			d.Parse(r.body.c_str());
			
			Value& count = d["count"];
			if (count.GetInt() == COMMENT_CASE_SIZE)
				return false;
			int commentsCount = count.GetInt() + 1;
			count.SetInt(commentsCount);
			
			Value& comments = d["comments"];
			Document::AllocatorType& allocator = d.GetAllocator();
			comments.PushBack(StringRef(text.c_str()), allocator);
			
			r = RestClient::put(caseDbPath, "", getStringFromDocument(d));
			if (r.code != HTTP_CREATED)
				return false;
			if (commentsCount == COMMENT_CASE_SIZE)
				updatePackageNumber(plcCmtPkgId, caseNum);
			return true;
		}
		
		void updatePackageNumber(string* packageId, int currentCaseNum)
		{
			string packagePath = CouchDbServer + COMMENT_PACKAGES + (*packageId);
			RestClient::Response r = RestClient::get(packagePath);
			Document d;
			d.Parse(r.body.c_str());
			if (d["curNum"].GetInt() != currentCaseNum)
				return;
			d["curNum"].SetInt(currentCaseNum + 1);
			
			//try create new comment_case
			createNewCommentCase(packageId, currentCaseNum + 1);
			Value& cases = d["cases"];
			cases.PushBack(StringRef((COMMENTS + (*packageId) + "/" + to_string(currentCaseNum + 1)).c_str()), d.GetAllocator());
			RestClient::put(packagePath, "", getStringFromDocument(d));
		}
		
		void createNewCommentCase(string* packageId, int caseNumber)
		{
			string caseDbPath = CouchDbServer + "/comment_cases/" +(*packageId) + "_" + to_string(caseNumber);
			Document d;
			d.Parse("{}");
			Document::AllocatorType& allocator = d.GetAllocator();
			
			string s = COMMENTS + (*packageId) + "/" + to_string(caseNumber);
			Value selfUri(s.c_str(), s.length(), allocator);
			d.AddMember("selfUri", selfUri, allocator);
			Value count;
			count.SetInt(0);
			d.AddMember("count", count, allocator);
			Value nextCaseUri;
			nextCaseUri.SetString(StringRef((COMMENTS + (*packageId) + "/" + to_string(caseNumber + 1)).c_str()));
			d.AddMember("nextCaseUri", nextCaseUri, allocator);
			Value comments;
			comments.SetArray();
			d.AddMember("comments", comments, allocator);
			RestClient::put(caseDbPath, "", getStringFromDocument(d));
		}
		
		string getStringFromDocument(Document &doc)
		{
			StringBuffer buffer;
			Writer<StringBuffer> writer(buffer);
			doc.Accept(writer);
			return buffer.GetString();
		}
		
		int getCaseNumber(string* plcCmtPkgId)
		{
			Document d;
			RestClient::Response r = RestClient::get(CouchDbServer + COMMENT_PACKAGES + *plcCmtPkgId);
			if (r.code != HTTP_OK)
				if (!createCommentsPackage(plcCmtPkgId))
					return PLACE_NOT_EXISTS;
			r = RestClient::get(CouchDbServer + COMMENT_PACKAGES + *plcCmtPkgId);
			d.Parse(r.body.c_str());
			int number = d["curNum"].GetInt();
			return number;
		}
		
		bool createCommentsPackage(string* packageId)
		{
			string caseDbPath = CouchDbServer + COMMENT_PACKAGES +(*packageId);
			RestClient::Response r = RestClient::get(CouchDbServer + DB_PLACES + (*packageId)); 
			if (r.code != HTTP_OK)
				return false;
			Document d;
			d.Parse("{}");
			Document::AllocatorType& allocator = d.GetAllocator();
			string s = COMMENTS + (*packageId);
			Value selfUri(s.c_str(), s.length(), allocator);
			d.AddMember("selfUri", selfUri, allocator);
			Value curNum;
			curNum.SetInt(1);
			d.AddMember("curNum", curNum, allocator);
			Value placeUri;
			placeUri.SetString(StringRef((COMMENTS + (*packageId)).c_str()));
			d.AddMember("placeUri", placeUri, allocator);
			Value cases;
			cases.SetArray();
			cases.PushBack(StringRef((COMMENTS + (*packageId) + "/" + to_string(1)).c_str()), d.GetAllocator());
			d.AddMember("cases", cases, allocator);
			createNewCommentCase(packageId, 1);
			RestClient::put(caseDbPath, "", getStringFromDocument(d));
			return true;
		}
		
		vector<string> split(const string &text, char sep) 
		{
			vector<string> tokens;
			size_t start = 0, end = 0;
			while ((end = text.find(sep, start)) != string::npos) {
				if (start != end)
					tokens.push_back(text.substr(start, end - start));
				start = end + 1;
			}
			if (text[start] != '\0')
				tokens.push_back(text.substr(start));
			return tokens;
		}
};
#endif